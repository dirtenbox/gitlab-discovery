export default {
  list: [
    {title: 'None', file: ''},
    {title: 'GNU GPLv3', file: 'gpl3.txt'},
    {title: 'MIT License', file: 'mit.txt'},
    {title: 'Apache License 2.0', file: 'apache.txt'},
    {title: 'separator', file: ''},
    {title: 'BSD 2-Clause "Simplified" License', file: 'bsd2.txt'},
    {title: 'BSD 3-Clause "New" or "Revised" License', file: 'bsd3.txt'},
    {title: 'Eclipse Public License 2.0', file: 'epl-2.txt'},
    {title: 'GNU AGPLv3', file: 'agpl3.txt'},
    {title: 'GNU GPLv2', file: 'gpl2.txt'},
    {title: 'GNU LGPLv2.1', file: 'lgpl21.txt'},
    {title: 'GNU LGPLv3', file: 'lgpl.txt'},
    {title: 'Mozilla Public License 2.0', file: 'mpl.txt'},
    {title: 'The Unlicense', file: 'unlicense.txt'}
  ]
}
